var firstTime = false;
jQuery(document).ready(function ($) {
    "use strict";
    var $t = true;
    var responsiveHtml='<div class="cd-dropdown-wrapper" id="massive-mobile-setting-drop">' +
        '<nav class="cd-dropdown">' +
        '<ul>' +
        '<li class="desktop"><i class="dropdown-icon icon-paper"></i><div class="circle expand"></div><a href="#"><span>'+customizerSentences.desktop+'</span><span class="cd-dropdown-option"></span></a></li>' +
        '<li class="tablet-portrait"><i class="dropdown-icon icon-paper"></i><div class="circle"></div><a href="#"><span>'+customizerSentences.tabletPortrait+'</span><span class="cd-dropdown-option"></span></a></li>' +
        '<li class="smartphone-portrait"><i class="dropdown-icon icon-paper"></i><div class="circle"></div><a href="#"><span>'+customizerSentences.mobilePortrait+'</span><span class="cd-dropdown-option"></span></a></li>' +
        '</ul>' +
        '</nav>' +
        '</div>';



    var informationHtml='<div class="cd-dropdown-wrapper">' +
        '<nav class="cd-dropdown">' +
        '<ul>' +
        '<li class="drop-down-title">'+customizerSentences.information+'</li>'+
        '<li><i class="dropdown-icon icon-paper"></i><div class="circle"></div><a target="_blank" href="https://help.massivedynamic.co/hc/en-us"><span>'+customizerSentences.helpAndSupportCenter+'</span><span class="cd-dropdown-option"></span></a></li>' +
        '<li><i class="dropdown-icon icon-paper"></i><div class="circle"></div><a target="_blank" href="http://massivedynamic.co/documentation/"><span>'+customizerSentences.documentation+'</span><span class="cd-dropdown-option"></span></a></li>' +
        '</ul>' +
        '</nav>' +
        '<div class="helper"></div>'+
        '</div>';

    var settingHtml='<div class="cd-dropdown-wrapper" id="massive-page-setting-drop">' +
        '<nav class="cd-dropdown">' +
        '<ul>' +
        '<li class="general-page-setting"><i class="dropdown-icon icon-paper"></i><div class="circle"></div><a href="#"><span>'+customizerSentences.generalPageSetting+'</span><span class="cd-dropdown-option"></span></a></li>' +
        '<li class="unique-page-setting"><i class="dropdown-icon icon-paper"></i><div class="circle"></div><a href="#"><span>'+customizerSentences.uniquePageSetting+'</span><span class="cd-dropdown-option"></span></a></li>' +
        '<li class="description"><a>'+customizerSentences.pageSettingDescription+'</a></li>' +
        '</ul>' +
        '</nav>' +
        '</div>';


    var newsletterHtml='<ul class="newsletter-content"></ul>' ;
    var takeTourHtml= '<ul class="newsletter-content">' +
        '<li>'+
        '<div class="take-tour-img"></div>'+
        '<p class="take-tour-title">'+customizerSentences.needHelp+'</p>'+
        '<p class="take-tour-text">'+customizerSentences.noWorries+' <a target="_blank" href="https://www.youtube.com/watch?v=LRWMd7M8VKs">'+customizerSentences.video+'</a> '+customizerSentences.takeATourDescription+'</p>'+
        '<div><a href="#" class="take-tour-btn">'+customizerSentences.takeATour+'</a></div>'+

        '<div class="help-center-container2">'+
        '<a href="#" class="show-hints hints-toggle">'+customizerSentences.showHints+'</a>'+
        '<a href="#" class="hide-hints">'+customizerSentences.hideHints+'</a>'+
        '</div>'+

        '</li>' +
        '</ul>';

    var notifyCenter='<div class="title notification-center close" style="opacity:1;">'+
        '<div id="notification-tabs">'+
        '<div class="pager"></div>'+
        '<div class="tabs-container">'+
        '<div id="opt1" class="newsletter">'+
        '<div class="clearfix notification-tab">'+
        '<span class="tab-title">'+customizerSentences.newsletter+'</span>'+
        '<div class="newsletter-container">'+
        newsletterHtml+
        '</div>'+
        '</div>'+
        '</div>'+
        '<div id="opt2" class="take-a-tour">'+
        '<div class="notification-tab clearfix">'+
        '<span class="tab-title">'+customizerSentences.takeTour+'</span>'+
        '<div class="take-a-tour-container">'+
        takeTourHtml+
        '</div>'+
        '</div>'+
        '</div>'+
        '</div>'+
        '<div class="notification-collapse-area"></div>'+
        '<div class="notification-collapse"></div>'+
        '</div>'+
        '</div>';

    var tourHtml='<div class="tour-dropdown-wrapper">' +
        '<div class="tour-dropdown">' +
        "<div class='tour-dropdown-header' style='width:270px;margin-bottom:22px; margin-top:30px; padding-bottom:70px'><a class='notify-remove px-icon icon-close'></a></div>"+
        notifyCenter+
        '</div>' +
        '</div>';


    $('.wp-full-overlay-sidebar-content,.accordion-section-content,.caroufredsel_wrapper').niceScroll({
        horizrailenabled: false,
        cursorcolor: "rgba(204, 204, 204, 0.2)",
        cursorborder: "1px solid rgba(204, 204, 204, 0.2)",
        cursorwidth: "2px"
    });

    $('#customize-info').css({display: 'none'});
    $('#customize-header-actions').before("<div class='customizer-header'><div class='pic'><a class='notify-win'>"+customizerSentences.avatarImage+"</a></div>" +
        "<p class='name'>"+customizerSentences.fullname+"</p></div>");
    $('.customize-controls-close').append('Close');
    var settingSvg = '<span class="mdb-settingsvg-2" > </span>',
        phoneSvg = '<span class="mdb-phone" > </span>';
		var $addPage =    '<div class="customizer-btn page"><span class="symbol">Add New</span>' +
                '<div class="tooltip">' +
                '<span class="title">'+customizerSentences.newEntries+'</span> '+customizerSentences.addNewPosts+' ' +
                '</div>' +
                '<div class="cd-dropdown-wrapper">' +
                '<nav class="cd-dropdown">' +
                '<ul>'+
                '<li data-type="Post" class="add-new-element"><i class="dropdown-icon icon-paper"></i><div class="circle"></div><a><span>'+customizerSentences.addNewPost+'</span><span class="cd-dropdown-option"></span></a></li>' +
                '<li data-type="Page" class="add-new-element"><i class="dropdown-icon icon-paper"></i><div class="circle"></div><a><span>'+customizerSentences.addNewPage+'</span><span class="cd-dropdown-option"></span></a></li>' +
                '<li data-type="Portfolio" class="add-new-element"><i class="dropdown-icon icon-paper"></i><div class="circle"></div><a><span>'+customizerSentences.addNewPortfolio+'</span><span class="cd-dropdown-option"></span></a></li>'+
                '<li><i class="dropdown-icon icon-paper"></i><div class="circle"></div><a target="_blank" href="'+customizerSentences.adminURL+'post-new.php?post_type=product"><span>'+customizerSentences.addNewProduct+'</span><span class="cd-dropdown-option"></span></a></li>'+
                '</ul>'+
                '</nav>' +
                '</div>' +
            '</div>' ;
	$('#customize-preview').prepend(
            '<div class="customizer-options-menu">'+
            '<a class="customizer-btn setting" id="page-option-btn"><span class="symbol">'+settingSvg+'</span><span class="text"></span><div class="tooltip"><span class="title">'+customizerSentences.generalUniqueSetting+'</span> '+customizerSentences.switchGeneralUnique+' </div><div class="save-loading"></div></a>' +
            '<a class="customizer-btn responsive-view"><span class="symbol">'+phoneSvg+'</span><div class="tooltip"><span class="title">'+customizerSentences.responsiveView+'</span> '+customizerSentences.differentDevices+' </div></a>' +
            '<a class="customizer-btn import"><span class="symbol">Demos</span><div class="tooltip"><span class="title">'+customizerSentences.importDemo+'</span> '+customizerSentences.usePremadeWebsites+' </div></a>' +
         // ADD ADD NEW BUTTON HERE IF NESSERY ALSO
            '<a class="customizer-btn dashboard" href="'+customizerSentences.adminURL+'" ><span class="symbol"></span><span class="text">'+customizerSentences.dashboard+'</span><div class="tooltip"><span class="title">'+customizerSentences.dashboardController+'</span>'+customizerSentences.goToDashboard+'</div></a>' +
            '<a class="collaps customizer-btn"><span class="symbol" >Preview</span><div class="tooltip"><span class="title">'+customizerSentences.gizmoController+'</span> '+customizerSentences.showHideControllers+' </div></a>' +
            '<div class="customizer-btn" id="save-btn"><span class="text"> '+customizerSentences.saveAndView+'</span><div class="save-loading"></div></div></div> ' );



        //'<a class="customizer-btn information"><span class="symbol"></span><div class="tooltip"><span class="title">'+customizerSentences.informationController+'</span>'+customizerSentences.needInformation+'</div></a>' +

    var pageTitle;
    $('.customizer-btn .add-new-element').click(function(){

        var postType,$promptBox,$close,$createPage;

        postType = $(this).attr('data-type');

        $('.prompt-box-wrapper').remove();
        $promptBox = $('' +
            '<div class="prompt-box-wrapper">' +
            '   <div class="prompt-box-container ">' +
            '       <div class="prompt-box-close"/>' +
            '       <div class="prompt-box-title">'+customizerSentences.addNew+' ' + postType + '</div>' +
            '       <div class="prompt-box-text">'+customizerSentences.enterName+' ' + postType.toLowerCase() + '</div>' +
            '       <input type="text" name="pageTitle" placeholder="' + postType + ' Name" class="prompt-text-box"><br/>'+
            '       <div class="prompt-box-holder"><a href="#" class="prompt-box-btn">'+customizerSentences.create+'</a></div>' +
            '   </div>' +
            '</div>').appendTo('body');

        $promptBox.animate({opacity:1},200);

        $close = $promptBox.find('.prompt-box-close');
        $close.click(function(){
            $('.cd-dropdown ul li .circle').removeClass('expand');
            $('.prompt-box-wrapper').fadeOut(300,function(){
                $(this).remove();
            });
        });

        $createPage = $promptBox.find('.prompt-box-btn');
        $createPage.click(function(){
            $('.cd-dropdown ul li .circle').removeClass('expand');
            pageTitle=$promptBox.find('.prompt-text-box').val();
            $('.prompt-box-wrapper').fadeOut(500,function(){
                $(this).remove();
            });

            if ( pageTitle == '' ) {
                return false;
            }
            jQuery.ajax({
                type: "post",
                url: customizerSentences.ajaxURL,
                data: "action=pixflow_addNewElement" +
                "&nonce=" + customizerSentences.ajaxNonce +
                "&pageTitle=" + pageTitle +
                "&postType=" + postType.toLowerCase() ,
                success: function (result) {
                    pixflow_customizerLoading();
                    window.wp.customize.previewer.previewUrl(result);
                }
            });
        });
    });

    $('#customize-preview .customizer-btn:not(.save-loading)').mouseenter(function(){

        $(this).find('.tooltip').css('display','block');

        if($(this).hasClass('dashboard') || $(this).attr('id')=="save-btn"){

            $(this).find('.tooltip').stop(true).delay(4000).animate({
                top: '15px',
                opacity: 1
            }, 250);

        }else{
            $(this).find('.tooltip').stop(true).delay(4000).animate({
                top: '65px',
                opacity: 1
            }, 250);
        }

    });
    $('#customize-preview .customizer-btn:not(.save-loading)').mouseleave(function(){

        if($(this).hasClass('dashboard') || $(this).attr('id')=="save-btn"){

            $(this).find('.tooltip').stop(true).animate({
                top: '5px',
                opacity: 0
            }, 300, function() {
                $(this).css('display','none');
            });

        }else{

            $(this).find('.tooltip').stop(true).animate({
                top: '40px',
                opacity: 0
            }, 300, function() {
                $(this).css('display','none');
            });

        }

    });

    $('.wp-full-overlay-sidebar').append(tourHtml);
    var tourDropdownToggle = -1;
    $('.customizer-header:first .pic').click(function(){
        return ;
        if(tourDropdownToggle != -1) return;
        tourDropdownToggle = 1;
        $('.tour-dropdown-wrapper').addClass('active-dropdown-view');
        $('.tour-dropdown').css({'opacity':0,'margin-top':'-=20px'}).delay(100).animate({
            opacity:1,
            'margin-top':'+=20px'
        },500);
        $('.customizer-header .notify-num').remove();
        if(!$('.tour-dropdown-wrapper .pager a:eq(1)').hasClass('selected')){
            pixflow_setAsReadNotifications();
        }
    });
    $('body').on('click','.tour-dropdown-wrapper .pager a:eq(0)',function(){
        pixflow_setAsReadNotifications();
    })
    $('body').on('click','.tour-dropdown-wrapper li .remove-notify',function(){
        var heightLi=parseInt($(this).parent().height())+33,
            $this=$(this);
        TweenMax.to($this.parent(),.2, {'opacity':'0'});
        TweenMax.to($this.parent(),.4, {'margin-top':'-'+heightLi+'px'});
        setTimeout(function(){
            pixflow_deleteNotification($this.parent().attr('data-notify-id'));
            $this.parent().remove();
        },800);

    });

    var notifyCountDown,responsiveCountDown,saveCountDown,pageCountDown,informationCountDown,settingCountDown;
    $('.notify-remove').click(function(e){
        e.stopPropagation();
        notifyCountDown = setTimeout(function() {
            $('.tour-dropdown-wrapper').removeClass('active-dropdown-view').css('z-index',10);
            $('.bullet.new').removeClass('new');
            $('.tour-dropdown-wrapper .notify-new').remove();
            setTimeout(function() {
                $('.tour-dropdown-wrapper').css('z-index','');
                tourDropdownToggle = -1;
            },300)
        }, 10);
    });
    $(document).click(function (e)
    {
        var container = $(".tour-dropdown-wrapper, .customizer-header");

        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            $('.notify-remove').click();
        }
    });

    $('#notification-tabs .tabs-container').carouFredSel({
        circular: false,
        items: 1,
        width: '100%',
        direction: "left",
        auto: false,
        pagination: {
            container: '#notification-tabs .pager',
            anchorBuilder: function() {
                var tabTitle= '<a href="#" class="'+  $(this).find('.tab-title').text().toLowerCase().replace(/ /g,'-') +'">' + $(this).find('.tab-title').text() + '</a>';

                return tabTitle;
            }
        },
        scroll : {
            fx : "directscroll",
            easing : "quadratic",
            duration:600,
            onBefore : function(data){
                data.items.old.css({opacity:0.5});
                data.items.old.stop().animate({opacity:0},300);
                data.items.visible.stop().animate({opacity:1},800);
            }
        }
    });

    $('#notification-tabs .tabs-container').css('width','204px');

   /* $('#save-btn').append(saveHtml);
    $('#save-btn').click(function(){
        $(this).find('.cd-dropdown-wrapper').addClass('active-dropdown-view');
        $(this).find('.cd-dropdown-wrapper .helper').css({'background-image': "url('"+customizerSentences.customizerURI+"/assets/images/save-&-publish.gif')", 'background-position':'center center'});
    });*/

    $('.responsive-view').append(responsiveHtml);
    $('.responsive-view').click(function(){
        $(this).find('.cd-dropdown-wrapper').addClass('active-dropdown-view');
    });

    $('.customizer-btn.information').append(informationHtml);
    $('.customizer-btn.information').click(function(){
        $(this).find('.cd-dropdown-wrapper').addClass('active-dropdown-view');
    });

    $('.customizer-btn.setting').append(settingHtml);
    $('.customizer-btn.setting').click(function(){
        $(this).find('.cd-dropdown-wrapper').addClass('active-dropdown-view');
        // Show hint step 1
        $('.introjs-hint[data-step=1]').css('opacity', 1);
    });

    $('.customizer-btn.page').click(function(){
        $(this).find('.cd-dropdown-wrapper').addClass('active-dropdown-view');
    });

    var responsiveCountDown,saveCountDown,pageCountDown,informationCountDown,settingCountDown,
        pannelFlag=false, hintFlag=true ;


    function pixflow_hidePannel() {
        'use strict';
        if ( pannelFlag && hintFlag ) {
            $('body').click();
            $('.introjs-hint[data-step=1]').css('display', 'none');
            $('.customizer-btn.setting .cd-dropdown-wrapper').removeClass('active-dropdown-view');
        }

    }

    $('.customizer-btn.setting').mouseleave(function() {

        settingCountDown = setTimeout(function () {

            pannelFlag = true;

            pixflow_hidePannel();

        }, 500);

    });


    $('.customizer-btn.setting').mouseenter(function() {
        pannelFlag = false;
    });


    /* Hint mouse enter & leave */
    $("body").on('mouseenter', '.introjs-hint[data-step=1]', function() {
        hintFlag = false;
    });

    $("body").on('mouseup', '.introjs-button', function() {
        hintFlag = true;
    });

    $("body").on('mouseleave', '.introjs-hint[data-step=1]', function() {
        hintFlag = true;
    });

    /* Tooltip mouse enter */
    $("body").on('mouseenter', '.introjs-fixedTooltip[data-step=1] .introjs-tooltip', function() {
        hintFlag = false;
    });

    /* Tooltip mouse leave */
    $("body").on('mouseleave', '.introjs-fixedTooltip[data-step=1] .introjs-tooltip', function() {
        hintFlag = true;
        $('body').click();
        pixflow_hidePannel();
    });

    // Video link clicked
    $('body').on('mouseup','.video-link-part', function() {
        hintFlag = true;
    });



    $('.customizer-btn.setting').mouseover(function() {
        clearTimeout(settingCountDown);
    });

    $('.responsive-view').mouseleave(function(){
        responsiveCountDown = setTimeout(function() {
            $('.responsive-view .cd-dropdown-wrapper').removeClass('active-dropdown-view');
        }, 500);
    });
    $('.responsive-view').mouseover(function() {
        clearTimeout(responsiveCountDown);
    });

    /*$('#save-btn .save-preview').click(function () {
        $('#save-btn .cd-dropdown-wrapper').removeClass('active-dropdown-view');
        $('#save-btn .cd-dropdown-wrapper .circle').removeClass('expand');
    });*/

    $('#save-btn').mouseleave(function(){
        saveCountDown = setTimeout(function() {
            $('#save-btn .cd-dropdown-wrapper .circle').css('transition-duration','1ms');
        }, 500);
    });

    $('#save-btn').mouseover(function() {
        clearTimeout(saveCountDown);
    });

    $('.customizer-btn.page').mouseleave(function(){
        pageCountDown = setTimeout(function() {
            $('.customizer-btn.page .cd-dropdown-wrapper').removeClass('active-dropdown-view');
        }, 500);
    });
    $('.customizer-btn.page').mouseover(function() {
        clearTimeout(pageCountDown);
    });

    $('.customizer-btn.information').mouseleave(function(){
        informationCountDown = setTimeout(function() {
            $('.customizer-btn.information .cd-dropdown-wrapper').removeClass('active-dropdown-view');
        }, 500);
    });
    $('.customizer-btn.information').mouseover(function() {
        clearTimeout(informationCountDown);
    });

    $('body').click(function(){
        $('.cd-dropdown-wrapper ul li .cd-dropdown-option').css({'background-image':'none'});
        $('.cd-dropdown-wrapper ul li a').css('color','#262626');
    });

    $('.cd-dropdown-wrapper ul li:not(.description)').click(function(e){
        e.stopPropagation();
        $('.cd-dropdown-wrapper ul li .cd-dropdown-option').css({'background-image':'none'});
        $(this).find('.cd-dropdown-option').css({
            'z-index':1,
            'position': 'relative',
            'text-indent': '90px',
            'transform':'rotateY(90deg)'
        }).animate({'text-indent':0},{
            step:function(now,fx){
                $(this).css('transform','rotateY('+ now +'deg)')
            }
        },800);

        var parentOffset = $(this).offset();
        //or $(this).offset(); if you really just want the current element's offset
        var relX = e.pageX - parentOffset.left;
        var relY = e.pageY - parentOffset.top;

        var speed = (Math.abs($(this).width()/2 - relX )+1);

        if(speed < 50) speed = 3500;
        else if(speed < 90) speed = 2000;
        else speed = 1000;
        if($(this).hasClass('desktop') || $(this).hasClass('tablet-portrait') || $(this).hasClass('smartphone-portrait')) {
            $('.cd-dropdown ul li:not(.general-page-setting,.unique-page-setting) .circle').removeClass('expand');
        }else{
            $('.cd-dropdown ul li:not(.desktop,.tablet-portrait,.smartphone-portrait) .circle').removeClass('expand');
        }

        $(this).find('.circle').addClass('expand');
        $(this).find('.circle').css('top',relY);
        $(this).find('.circle').css('left',relX);
        $('.cd-dropdown ul li .circle').css('transition-duration','0ms');
        $(this).find('.circle').css('transition-duration',speed+'ms');

        if($(this).hasClass('desktop')) {
            $('body iframe').contents().find('body').removeClass('mobile-portrait-mode tablet-portrait-mode responsive-mode').addClass('desktop-mode');
            $('button.preview-desktop').click();

        }else if($(this).hasClass('tablet-portrait')) {
            $('body iframe').contents().find('body').removeClass('desktop-mode mobile-portrait-mode').addClass('tablet-portrait-mode responsive-mode');
            $('body iframe').contents().find('.responsive-col-50,.responsive-full-width').each(function(){
                var $this = $(this);
                if ($this.hasClass('responsive-col-50')){
                    $this.removeClass('responsive-col-50');
                    $this.closest('.vc_vc_column').addClass('responsive-col-50');
                }else{
                    $this.removeClass('responsive-full-width');
                    $this.closest('.vc_vc_column').addClass('responsive-full-width');
                }

            });
            $('button.preview-tablet').click();
            if(!$('.collaps.customizer-btn').hasClass('hold-collapse')){
                $('.collaps.customizer-btn').click();
            }

        }else if($(this).hasClass('smartphone-portrait')) {
            $('body iframe').contents().find('body').removeClass('desktop-mode tablet-portrait-mode responsive-mode').addClass('mobile-portrait-mode responsive-mode');
            $('button.preview-mobile').click();
            if(!$('.collaps.customizer-btn').hasClass('hold-collapse')){
                $('.collaps.customizer-btn').click();
            }
        }
    });

    $('#customize-preview .customizer-btn').click(function(){
        $(this).find('.tooltip').css({'display':'none','top':'5px','opacity':'0'});
    });
    $('#customize-preview .customizer-btn .cd-dropdown-wrapper').mousemove(function(){
        $(this).prev().css({'display': 'none','top': '5px','opacity':'0'});
    });


    var customizerLoader = $('<div class="main-loader">' + 
	'<div class="showbox_loading"><div class="loader_loading">' +
            '<svg class="circular_loading" viewBox="25 25 50 50">' +
      '<circle class="path_loading" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>' +
            ' </svg><span>loading</span></div></div>' +
	'<div/>');
    $('.customizer-loading').css('display' , 'block');
    customizerLoader.appendTo('body');
});
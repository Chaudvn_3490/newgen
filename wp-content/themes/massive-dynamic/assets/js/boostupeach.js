/*
 * BoostUpEach
 * http://cloudypix.com/
 *
 * v1.0.0
 */
(function($){$.fn.boostUpEach=function(callback){var index=0,t=this;boostEach(0);function boostEach(index){if(!t.length||t.length==index){return;}callback.apply(t[index],[index]);setTimeout(function(){boostEach(index+1)},0);}};})(jQuery);
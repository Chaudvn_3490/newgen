<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
define('AUTOSAVE_INTERVAL', 300 ); // seconds
define('WP_POST_REVISIONS', false );
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bluecube_newgen');

/** MySQL database username */
define('DB_USER', 'bluecube_newgen');

/** MySQL database password */
define('DB_PASSWORD', 'Passw0rd456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'f;P,,56i>KW^k#H+ZE_tD&t4C;P+}==C[TrJrLTsUF3k]O_U0uVFy&RRYW;}DvNX');
define('SECURE_AUTH_KEY',  '>mnZU9:7c[EkMsD {h%_r_m7~[KoO8r(Zhv LeC0DQwD-xHwdyyidG&4gu1/3d7@');
define('LOGGED_IN_KEY',    '.k`sv9~F{8a>#zUtR+g@&/-m3dB56)]>V)PSd3Y*D=.-xY?_xs_>/$:A6,>G@!MI');
define('NONCE_KEY',        '<TyN2RHlHy{9rT@oIn$I&?tH2yx?BLL[+s45lYewm@eP{wo?W)jWUj&N:8mL`n8g');
define('AUTH_SALT',        'Ov!5En7G6^_Dz,Oi#Fs/YGtP[Wj=<9u)EqZ90Mtu{y)lePuo&FBJp*G@F$huNci(');
define('SECURE_AUTH_SALT', '=J(2kn471fz@bULX#*e1aoocFz{[lRyKIUQMlEiQLiw?MbA}Jj-@TsZqI}X{kGZ%');
define('LOGGED_IN_SALT',   'GJ:oT6aUDO{ Fcs7I2#$~5<CU](0F8{}}G_2FP9E%(z+lLf O~i@q>d)Y^*]?1Q!');
define('NONCE_SALT',       'Ac+~!lsC+3Xb~mHa:I64we$VU?*ND6M!ZbzQJtG40Z?+mtv,aUV>uq!]l}oiM5Xk');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
